#!/usr/bin/python3
# * Copyright 2016-2017 Dmitry Nikolaev
# * Licensed with GNU/GPLv3, see LICENSE file
# * This file is part of getproxy

# * getproxy is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.

# * getproxy is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# * GNU General Public License for more details.

# * You should have received a copy of the GNU General Public License
# * along with getproxy. If not, see <http://www.gnu.org/licenses/>.
import leaf
import urllib.request
from requests import get
import json
class Proxy:
  def __init__(self, ip, port):
    self.ip = ip
    self.port = port

class ProxyList:
  proxies = []
  def addPage(self, j):
    self.pages+=1 
    proxyurl = "https://hidemy.name/en/proxy-list/?start="
    proxyGet = urllib.request.Request(proxyurl+str(j*64))
    proxyGet.add_header('user-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/33.0')
    document = leaf.parse(urllib.request.urlopen(proxyGet).read())
    for i in range(len(document('td.tdl'))):
      cip = document('td.tdl')[i]
      cport = cip.getparent()('td')[1]
      self.proxies.append(Proxy(cip, cport))
  def __len__(self):
    return len(self.proxies)
  def __init__(self, cntPages = 1):
    self.pages = 0
    self.currProxy = 0
    for j in range(cntPages):
      self.addPage(j)
  def checkProxy(self, obj):
    pr = {'http': 'http://{}:{}/'.format(obj.ip, obj.port)}
    try:
      if(str(obj.ip) == json.loads(get('http://ip-api.com/json', proxies=pr, timeout=5).text)['query']):
        return True
    except:
      return False
  def updateProxyList(self, cntPages = 1):
    self.__init__(cntPages)
  def getValidProxy(self):
    for i in range(self.currProxy, len(self.proxies)-1):
      if(self.checkProxy(self.proxies[i])):
        self.currProxy = i+1
        return self.proxies[i]
      #if(i==len(self.proxies)-2):
        #print("updating..")
        #self.addPage(self.pages+1) 
  def getProxyList(self):
    return self.proxies
